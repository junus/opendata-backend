<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Country;
use AppBundle\Entity\ExportData;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

class ExportDataRepository extends EntityRepository {
    public function getDataByCountry(Country $country, $products) {
        $qb = $this->createQueryBuilder('d');
        $params['pc']   = $country;
        $params['type'] = ExportData::TYPE_PARTNER;
        $query = $qb->select("d.id, d.year, d.usd, p.title, p.id as product_id")
            ->join("AppBundle:Export", 'e', 'WITH', 'e.id = d.export')
            ->join("AppBundle:Product", 'p', 'WITH', 'p.id = e.product')
            ->where('e.partnerCountry = :pc', 'd.type = :type')
            ->orderBy('p.id, d.year');
        if (is_array($products)) {
            $query->andWhere($qb->expr()->in('e.product', ':pArr'));
            $params['pArr'] = $products;
        }
        $result = $query->setParameters($params)->getQuery();
        return $result->getArrayResult();
    }

    public function getDataByProduct(Product $product, $countries) {
        $qb = $this->createQueryBuilder('d');
        $params['pc']   = $product;
        $params['type'] = ExportData::TYPE_PARTNER;
        $query = $qb->select("d.id, d.year, d.usd, c.title, c.id as country_id")
            ->join("AppBundle:Export", 'e', 'WITH', 'e.id = d.export')
            ->join("AppBundle:Country", 'c', 'WITH', 'c.id = e.partnerCountry')
            ->where('e.product = :pc', 'd.type = :type')
            ->orderBy('c.id, d.year');
        if (is_array($countries)) {
            $query->andWhere($qb->expr()->in('e.partnerCountry', ':pArr'));
            $params['pArr'] = $countries;
        }
        $result = $query->setParameters($params)->getQuery();
        return $result->getArrayResult();
    }

    public function getSankeyData($years, $countries, $products) {
        $qb = $this->createQueryBuilder('d');
        $params['type'] = ExportData::TYPE_PARTNER;
        $query = $qb->select("p.title as product, c.title as country, d.usd, d.year")
            ->join("AppBundle:Export", 'e', 'WITH', 'e.id = d.export')
            ->join("AppBundle:Product", 'p', 'WITH', 'p.id = e.product')
            ->join("AppBundle:Country", 'c', 'WITH', 'c.id = e.partnerCountry')
            ->where('d.type = :type')
            ->orderBy('d.year');
        if (is_array($years)) {
            $query->andWhere($qb->expr()->in('d.year', ':years'));
            $params['years'] = $years;
        }
        if (is_array($countries)) {
            $query->andWhere($qb->expr()->in('c.id', ':countries'));
            $params['countries'] = $countries;
        }
        $query->andWhere($qb->expr()->in('e.product', ':pArr'));
        $params['pArr'] = $products;
        $result = $query->setParameters($params)->getQuery();
        return $result->getArrayResult();
    }
}