<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository {

    const ARMENIA = 2;
    const BELARUS = 3;
    const KAZAKHSTAN = 4;
    const RUSSIA = 7;

    public function getEaesCountries()
    {
        $qb = $this->createQueryBuilder('c');
        $result = $qb->where($qb->expr()->in('c.code', ":arr"))
            ->setParameter("arr", array(self::ARMENIA, self::BELARUS, self::KAZAKHSTAN, self::RUSSIA))
            ->getQuery();
        return $result->getArrayResult();
    }
}