<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\Product;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class ApiEaesController extends FOSRestController
{
    /**
     * Страны ЕВРАЗЭС
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCountriesAction()
    {
        $data = $this->getDoctrine()->getRepository("AppBundle:Country")->getEaesCountries();
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    /**
     * Продукты с фильтром по продуктам
     *
     * @param Request $request
     * @param $type
     * @param Country $country
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCountriesProductsAction(Request $request, $type, Country $country)
    {
        $products   = $request->query->get("products");
        if ($type == 'import') {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ImportData");
        } else {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ExportData");
        }
        $data = array();
        $results = $repository->getDataByCountry($country, $products);
        foreach ($results as $item) {
            if (!isset($data[$item['product_id']])) {
                $data[$item['product_id']] = [
                    'label' => $item['title'],
                    'values' => [],
                    'years' => []
                ];
            }
            $data[$item['product_id']]['values'][]  = $item['usd'];
            $data[$item['product_id']]['years'][]   = $item['year'];
        }

        $view = $this->view(array_values($data), 200);
        return $this->handleView($view);
    }

    /**
     * Продукт с фильтром по городам
     *
     * @param Request $request
     * @param $type
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductsByCountriesAction(Request $request, $type, Product $product)
    {
        $countries = $request->query->get("countries");
        if ($type == 'import') {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ImportData");
        } else {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ExportData");
        }
        $data = array();
        $results = $repository->getDataByProduct($product, $countries);
        foreach ($results as $item) {
            if (!isset($data[$item['country_id']])) {
                $data[$item['country_id']] = [
                    'label' => $item['title'],
                    'values' => [],
                    'years' => []
                ];
            }
            $data[$item['country_id']]['values'][]  = $item['usd'];
            $data[$item['country_id']]['years'][]   = $item['year'];
        }

        $view = $this->view(array_values($data), 200);
        return $this->handleView($view);
    }

    /**
     * Продукты
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductsAction()
    {
        $data = $this->getDoctrine()->getRepository("AppBundle:Product")->findAll();
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    /**
     * JSON для sankey диаграммы
     *
     * @param Request $request
     * @param $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sankeyAction(Request $request, $type){
        $products   = $request->query->get("products");
        $countries  = $request->query->get("countries");
        $years      = $request->query->get("years");
        if ($type == 'import') {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ImportData");
        } else {
            $repository = $this->getDoctrine()->getRepository("AppBundle:ExportData");
        }
        $result = $repository->getSankeyData($years, $countries, $products);
        $view = $this->view(array_values($result), 200);
        return $this->handleView($view);
    }
}
