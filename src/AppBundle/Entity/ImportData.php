<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImportData
 *
 * @ORM\Table(name="import_data", indexes={@ORM\Index(name="import_id", columns={"import_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImportDataRepository")
 */
class ImportData
{
    const TYPE_PARTNER          = 1;
    const TYPE_PARTNER_WORLD    = 2;
    const TYPE_WORLD            = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     */
    private $year;

    /**
     * @var float
     *
     * @ORM\Column(name="usd", type="float", precision=10, scale=0, nullable=false)
     */
    private $usd;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var Import
     *
     * @ORM\ManyToOne(targetEntity="Import")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="import_id", referencedColumnName="id")
     * })
     */
    private $import;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return ImportData
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set usd
     *
     * @param float $usd
     * @return ImportData
     */
    public function setUsd($usd)
    {
        $this->usd = $usd;

        return $this;
    }

    /**
     * Get usd
     *
     * @return float 
     */
    public function getUsd()
    {
        return $this->usd;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ImportData
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set import
     *
     * @param \AppBundle\Entity\Import $import
     * @return ImportData
     */
    public function setImport(\AppBundle\Entity\Import $import = null)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Get import
     *
     * @return \AppBundle\Entity\Import 
     */
    public function getImport()
    {
        return $this->import;
    }
}
