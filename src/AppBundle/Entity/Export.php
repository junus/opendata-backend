<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Export
 *
 * @ORM\Table(name="export", indexes={@ORM\Index(name="product_id", columns={"product_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExportRepository")
 */
class Export
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="partner_country", referencedColumnName="id")
     * })
     */
    private $partnerCountry;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partnerCountry
     *
     * @param Country $partnerCountry
     * @return Export
     */
    public function setPartnerCountry(Country $partnerCountry)
    {
        $this->partnerCountry = $partnerCountry;

        return $this;
    }

    /**
     * Get partnerCountry
     *
     * @return Country
     */
    public function getPartnerCountry()
    {
        return $this->partnerCountry;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return Export
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
