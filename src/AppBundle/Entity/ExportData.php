<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExportData
 *
 * @ORM\Table(name="export_data", indexes={@ORM\Index(name="export_id", columns={"export_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExportDataRepository")
 */
class ExportData
{
    const TYPE_PARTNER          = 1;
    const TYPE_PARTNER_WORLD    = 2;
    const TYPE_WORLD            = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     */
    private $year;

    /**
     * @var float
     *
     * @ORM\Column(name="usd", type="float", precision=10, scale=0, nullable=false)
     */
    private $usd;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var Export
     *
     * @ORM\ManyToOne(targetEntity="Export")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="export_id", referencedColumnName="id")
     * })
     */
    private $export;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return ExportData
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set usd
     *
     * @param float $usd
     * @return ExportData
     */
    public function setUsd($usd)
    {
        $this->usd = $usd;

        return $this;
    }

    /**
     * Get usd
     *
     * @return float 
     */
    public function getUsd()
    {
        return $this->usd;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ExportData
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set export
     *
     * @param \AppBundle\Entity\Export $export
     * @return ExportData
     */
    public function setExport(\AppBundle\Entity\Export $export = null)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return \AppBundle\Entity\Export 
     */
    public function getExport()
    {
        return $this->export;
    }
}
