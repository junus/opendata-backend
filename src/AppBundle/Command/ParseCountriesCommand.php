<?php

namespace AppBundle\Command;

use AppBundle\Entity\Country;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCountriesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:parse:countries')
            ->setDescription('parse countries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://opendata-hackathon.com:3000/foreign_trade_country');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get("doctrine.orm.default_entity_manager");
        $countryRepository = $em->getRepository("AppBundle:Country");

        $response = curl_exec($ch);
        $countries = json_decode($response);
        $batchSize = 20;
        $i = 1;
        foreach ($countries as $country) {
            if (!$exist = $countryRepository->findOneBy(array('code'=>$country->code))) {
                $newCountry = new Country();
                $newCountry->setCode($country->code);
                $newCountry->setTitle($country->title);
                $em->persist($newCountry);
                if (($i % $batchSize) === 0) {
                    $em->flush();
                    $em->clear();
                }
                $i++;
            }
        }
        $em->flush();
    }
}
